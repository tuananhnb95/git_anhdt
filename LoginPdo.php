<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
    <?php
    // Code PHP xử lý validate
        $error = array();
        $data = array();
        if (isset($_POST['login_action'])) {
            // Lấy dữ liệu
            $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
            $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';

            // Kiểm tra định dạng dữ liệu
            if (empty($data['email'])) {
                $error['email'] = 'Bạn chưa nhập email';
            } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $error['email'] = 'Email không đúng định dạng';
            } elseif (strlen($data['email']) > 255) {
                $error['email'] = 'Email không dài quá 255 ký tự';
            }

            if (empty($data['password'])) {
                $error['password'] = 'Bạn chưa nhập mật khẩu';
            } elseif (strlen($data['password']) < 6 || strlen($data['password']) > 50) {
                $error['password'] = 'Mật khẩu phải tối thiểu 6 đến 50 ký tự';
            }

            if (!isset($error['email']) && !isset($error['password'])) {
                $conn = new PDO("mysql:host=localhost; dbname=Anh_DT", 'root', '');
                $query = "SELECT * FROM users WHERE mail_address = ? and password = ?";
                $statement = $conn->prepare($query);
                $statement->bindValue(1, $data['email']);
                $statement->bindValue(2, $data['password']);
                $statement->execute();
                if ($statement->rowCount() > 0) {
                    session_start();
                    $_SESSION['email'] =  $data['email'];
                    $_SESSION['password'] = $data['password'];
                    // Kiểm tra remember me
                    if (isset($_POST['rmb'])) {
                        setcookie("email", $data['email'], time() +50000);
                        setcookie("password", $data['password'], time() +50000);
                        if (isset($_COOKIE['email'])) {
                            echo 'Tên Đăng Nhập Là: ' . $_COOKIE['email'];
                        } else {
                            echo 'Không tồn tại cookie';
                        }
                        echo 'Đăng nhập thành công<br>';
                        header("Location: LoginSuccessPdo.php");
                    } else {
                        setcookie("email", $data['email'], time() -50000);
                        setcookie("password", $data['password'], time() -50000);
                        header("Location: LoginSuccessPdo.php");
                    }
                } else {
                    $error['email'] = 'Sai thông tin email hoặc password';
                }
            } else {
                echo 'Đăng nhập thất bại<br>';
            }
        }
    ?>
    <div style="margin-top: 100px; margin-left: 350px; margin-right: 350px">
        <form method="POST" action="LoginPdo.php">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo isset($_COOKIE['email']) ? $_COOKIE['email'] : ''; ?>"/>
                <label style="color: red"><?php echo isset($error['email']) ? $error['email'] : ''; ?></label>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo isset($_COOKIE['password']) ? $_COOKIE['password'] : ''; ?>"/>
                <label style="color: red"><?php echo isset($error['password']) ? $error['password'] : ''; ?></label>
            </div>
            <div class="form-group">
                <input type="checkbox" name="rmb" <?php echo isset($_COOKIE['email']) ? 'checked': ''; ?> >Remember me
                <button style="margin-top: 30px; width: 180px" type="submit" class="btn btn-primary" name="login_action">Login</button>
            </div>
        </form>
    </div>
</body>
</html>