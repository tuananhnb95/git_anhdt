
<html>
<head>
    <title>Login Success</title>
    <meta charset="utf-8">
</head>
<body>
    <?php
        session_start();
        if (isset($_SESSION['email'])) {
            echo 'Tên Đăng Nhập Là: ' . $_SESSION['email'] . '<br>';
        } else {
            echo 'Không tồn tại session<br>';
        }

        if (!empty($_POST['logout_action'])) {
            unset($_SESSION['email']);
            header("Location: Login.php");
        }

    ?>
    <form method="POST" action="LoginSuccess.php">
        <input type="submit" name="logout_action" value="Logout"/>
        
    </form>
</body>
</html>