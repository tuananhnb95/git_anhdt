<!DOCTYPE html>
<html>
<head>
    <title>Thêm mới</title>
    <link rel="stylesheet" type="" "text/css" href="{{ asset('css/app.css') }}">
    <style type="text/css">
        .error{
            color: red;
        }
        .border{
            border:solid 1px red;
        }
    </style>
    <meta charset="utf-8">

</head>
<body>
    <div style="margin-top: 100px; margin-left: 350px; margin-right: 350px">
        <form method="POST" action="{{ route('users.store') }}">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-group">
                <label class="@if ($errors->has('mail_address')) {{'error'}} @endif">mail address</label>
                <input type="email" class="form-control @if ($errors->has('mail_address')) {{'border'}} @endif" name="mail_address" placeholder="Enter email">
                <label class="error">{{ $errors->first('mail_address') }}</label>
            </div>
            <div class="form-group">
                <label class="@if ($errors->has('password')) {{'error'}} @endif">Password</label>
                <input type="password" class="form-control @if ($errors->has('password')) {{'border'}} @endif" name="password" placeholder="Enter Password">
                <label class="error">{{ $errors->first('password') }}</label>
            </div>
            <div class="form-group">
                <label class="@if ($errors->has('password_confirmation')) {{'error'}} @endif">Password Comfirm</label>
                <input type="password" class="form-control @if ($errors->has('password_confirmation')) {{'border'}} @endif" name="password_confirmation" placeholder="Enter Password Comfirm">
                <label class="error">{{ $errors->first('password_confirmation') }}</label>
            </div>
            <div class="form-group">
                <label class="@if ($errors->has('name')) {{'error'}} @endif">Name</label>
                <input type="text" class="form-control @if ($errors->has('name')) {{'border'}} @endif" name="name" placeholder="Enter Name">
                <label class="error">{{ $errors->first('name') }}</label>
            </div>
            <div class="form-group">
                <label class="@if ($errors->has('address')) {{'error'}} @endif">Address</label>
                <input type="text" class="form-control @if ($errors->has('address')) {{'border'}} @endif" name="address" placeholder="Enter Address">
                <label class="error">{{ $errors->first('address') }}</label>
            </div>
            <div class="form-group">
                <label class="@if ($errors->has('phone')) {{'error'}} @endif">Phone</label>
                <input type="text" class="form-control @if ($errors->has('phone')) {{'border'}} @endif" name="phone" placeholder="Enter Phone">
                <label class="error">{{ $errors->first('phone') }}</label>
            </div>
            <button type="submit" class="btn btn-primary" name="create_action">Register</button>
        </form>
    </div>
</body>
</html>