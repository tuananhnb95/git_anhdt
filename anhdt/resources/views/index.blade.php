<html>
<head>
    <meta charset="utf-8">
    <title>Danh sách users</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="container">
        <table class="table table-bordered" style="margin: center">
            <thead style="text-align: center">
                <tr>
                    <th>STT</th>
                    <th>Địa chỉ email</th>
                    <th>Tên</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = ($users->currentpage()-1)*$users->perpage() +1; ?>
                @foreach($users as $user)
                    <tr>
                        <th>{{ $i++ }}</th>
                        <th>{{ $user-> mail_address }}</th>
                        <th>{{ $user->name }}</th>
                        <th>{{ $user-> address }}</th>
                        <th>{{ $user-> phone }}</th>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
        <a href="{{ route('users.create') }}" class="button">Thêm mới User</a>
    </div>
</body>
</html>
