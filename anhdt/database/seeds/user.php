<?php

use Illuminate\Database\Seeder;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create('vi_VN');

        for ($i = 0; $i < 100; $i++) {
            DB::table('users')->insert([
                'mail_address' => $faker->unique()->email,
                'password' => Hash::make($faker->password),
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->e164PhoneNumber,
            ]);
        }
    }
}
