
<html>
<head>
    <title>Login Success</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
    <?php
        session_start();
        if (isset($_SESSION['email'])) {
            echo 'Tên Đăng Nhập Là: ' . $_SESSION['email'] . '<br>';
        } else {
            echo 'Không tồn tại session<br>';
        }

        if (isset($_POST['logout_action'])) {
            unset($_SESSION['email']);
            header("Location: LoginPdo.php");
        }

    ?>
    <form method="POST" action="LoginSuccessPdo.php">
                <button style="margin-top: 30px; width: 180px" type="submit" class="btn btn-primary" name="logout_action">Log out</button>
            </div>
    </form>
</body>
</html>