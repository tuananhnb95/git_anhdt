<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
    <?php
        //     // Tạo DataBase và bảng
        // try {
        //     // Chuỗi kết nối
        //     $conn = new PDO("mysql:host=localhost", 'root', '');

        //     // Thiết lập chế độ exception
        //     $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //     // Câu truy vấn
        //     $sql = "CREATE DATABASE Anh_DT";

        //     // Thực thi câu truy vấn
        //     $conn->exec($sql);

        //     // Thông báo thành công
        //     echo "Tạo database thành công</br>";
        // } catch (PDOException $e) {
        //     echo $e->getMessage();
        // }

        // // Ngắt kết nối
        // $conn = null;

        // try {
        //     // Kết nối
        //     $conn = new PDO("mysql:host=localhost;dbname=Anh_DT", 'root', '');

        //     // Thiết lập chế độ exception
        //     $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //     // Câu lệnh SQL
        //     $sql = "CREATE TABLE users (
        //     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        //     mail_address VARCHAR(50) NOT NULL,
        //     password VARCHAR(50),
        //     created_at time,
        //     updated_at time
        //     )";

        //     // Thực thi câu truy vấn
        //     $conn->exec($sql);
     
        //     echo "Tạo table thành công</br>";
        // } catch (PDOException $e) {
        //     echo $e->getMessage();
        // }

        // // Ngắt kết nối
        // $conn = null;

        // Code xử lý validate
        $error = array();
        $data = array();

        if (isset($_POST['register_action'])) {

            // Lấy dữ liệu
            $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
            $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
            $data['password_confirm'] = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : '';

            //Kiểm tra định dạng
             if (empty($data['email'])) {
                $error['email'] = 'Bạn chưa nhập email';
            } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $error['email'] = 'Email không đúng định dạng';
            } elseif (strlen($data['email']) > 255) {
                $error['email'] = 'Email không dài quá 255 ký tự';
            }

            if (empty($data['password'])) {
                $error['password'] = 'Bạn chưa nhập mật khẩu';
            } elseif (strlen($data['password']) < 6 || strlen($data['password']) > 50) {
                $error['password'] = 'Mật khẩu phải tối thiểu 6 đến 50 ký tự';
            }

            if (empty($data['password_confirm'])) {
                $error['password_confirm'] = 'Bạn chưa nhập lại mật khẩu';
            } elseif ($data['password_confirm'] !== $data['password']) {
                $error['password_confirm'] = 'Mật khẩu không khớp';
            }

            // Thêm dữ liệu vào bảng users
            if (!isset($error['email']) && !isset($error['password']) && !isset($error['password_confirm'])) {
                try {
                // Tạo kết nối
                $conn = new PDO("mysql:host=localhost;dbname=Anh_DT", 'root', '');
                $query = $conn->prepare('INSERT INTO users (mail_address, password) VALUES (:mail, :password)');
                $query->bindParam(':mail', $data['email']);
                $query->bindParam(':password', $data['password']);
                $query->execute();
                echo "Register Success";
                header("Location: LoginPdo.php");
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }

        }

    ?>
    <div style="margin-top: 100px; margin-left: 350px; margin-right: 350px">
        <form method="POST" action="RegisterPdo.php">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="<?php echo isset($data['email']) ? $data['email'] : ''; ?>"/>
                <label style="color: red"><?php echo isset($error['email']) ? $error['email'] : ''; ?></label>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo isset($data['password']) ? $data['password'] : ''; ?>"/>
                <label style="color: red"><?php echo isset($error['password']) ? $error['password'] : ''; ?></label>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password Confirm</label>
                <input type="password" class="form-control" name="password_confirm" id="password_confirm" placeholder="Password Confirm" value="<?php echo isset($data['password_confirm']) ? $data['password_confirm'] : ''; ?>"/>
                <label style="color: red"><?php echo isset($error['password_confirm']) ? $error['password_confirm'] : ''; ?></label>
                <button style="margin-top: 30px;width: 180px" type="submit" class="btn btn-primary" name="register_action">Register</button>
            </div>
        </form>
    </div>
</body>
</html>