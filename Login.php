<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <?php
    // Code PHP xử lý validate
        $error = array();
        $data = array();
        if (!empty($_POST['login_action'])) {
            // Lấy dữ liệu
            $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
            $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';

            // Kiểm tra định dạng dữ liệu
            if (empty($data['email'])) {
                $error['email'] = 'Bạn chưa nhập email';
            } elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $error['email'] = 'Email không đúng định dạng';
            } elseif (strlen($data['email']) > 255) {
                $error['email'] = 'Email không dài quá 255 ký tự';
            }

            if (empty($data['password'])) {
                $error['password'] = 'Bạn chưa nhập mật khẩu';
            } elseif (strlen($data['password']) < 6 || strlen($data['password']) > 50) {
                $error['password'] = 'Mật khẩu phải tối thiểu 6 đến 50 ký tự';
            }

            if($data['email'] == 'anhdt@gmail.com' && $data['password'] == 'anhdt123') {
                session_start();
                $_SESSION['email'] =  'anhdt@gmail.com';
                $_SESSION['password'] = 'anhdt123';
                // Kiểm tra remember me
                if(isset($_POST['rmb'])) {
                    setcookie("email", $data['email'], time() +5000);
                    setcookie("password", $data['password'], time() +5000);
                    if (isset($_COOKIE['email'])) {
                        echo 'Tên Đăng Nhập Là: ' . $_COOKIE['email'];
                    } else {
                        echo 'Không tồn tại cookie';
                    }
                    echo 'Đăng nhập thành công<br>';
                    header("Location: LoginSuccess.php");
                } else {
                    setcookie("email", $data['email'], time() -5000);
                    setcookie("password", $data['password'], time() -5000);
                    header("Location: LoginSuccess.php");
                }     
            } else {
                echo 'Đăng nhập thất bại<br>';
            }
            
        }
       
    ?>
    <form method="POST" action="Login.php">
        <h1>Đăng nhập</h1>
            <table>
                <tr>
                    <td>Email</td>
                    <td>
                        <input type="mail" name="email" id="email" value="<?php echo isset($_COOKIE['email']) ? $_COOKIE['email'] : ''; ?>"/>
                    </td>
                    <td style="color:red">
                        <?php echo isset($error['email']) ? $error['email'] : ''; ?>
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" name="password" id="password" value="<?php echo isset($_COOKIE['password']) ? $_COOKIE['password'] : ''; ?>"/>
                    </td>
                    <td style="color:red">
                        <?php echo isset($error['password']) ? $error['password'] : ''; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="checkbox" name="rmb" <?php echo isset($_COOKIE['email']) ? 'checked': ''; ?> >Remember me
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"> <input type="submit" name="login_action" value="Login"></td>
                </tr>
            </table>
  </form>
</body>
</html>